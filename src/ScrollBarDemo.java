import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class ScrollBarDemo extends JPanel implements ChangeListener {
	
	int r, g, b;
	JSlider red = new JSlider(JSlider.HORIZONTAL, 0, 255, 127);
	JSlider green = new JSlider(JSlider.HORIZONTAL, 0, 255, 127);
	JSlider blue = new JSlider(JSlider.HORIZONTAL, 0, 255, 127);
	JLabel text = new JLabel("Show Colors");
	
	public ScrollBarDemo() {
		setSize(300, 200);
		//Creates panels
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		this.setLayout(new GridLayout(2, 1));
		panel1.setLayout(new GridBagLayout());
		panel2.setLayout(new FlowLayout());
		//Adds action listeners
		red.addChangeListener(this);
		green.addChangeListener(this);
		blue.addChangeListener(this);
		Listener listen = new Listener();
		panel2.setBorder(new CompoundBorder(new TitledBorder("Choose colors"),
				new EmptyBorder(2, 2, 2, 2)));
		panel1.add(text);
		panel2.add(red);
		panel2.add(green);
		panel2.add(blue);
		add(panel1);
		add(panel2);
	}

	//Action listener that listens to JSliders
	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource().equals(red)) {
			r = red.getValue();
		} else if (e.getSource().equals(green)) {
			g = green.getValue();
		} else if (e.getSource().equals(blue)) {
			b = blue.getValue();
		}
		text.setForeground(new Color(r, g, b));
	}
}

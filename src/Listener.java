import javax.swing.JFrame;

public class Listener extends JFrame{
	
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("Slider Demo");
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.add(new ScrollBarDemo());
		frame.setVisible(true);
	}
}
